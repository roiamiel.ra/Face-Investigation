import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        System.out.print("Face Investigation\n");

        BufferedImage bufferedImage = ImageIO.read(new File("Image.png"));

        displayImage("Image", bufferedImage);

        FaceInvestigator.Rect rect = new FaceInvestigator.Rect(190, 70, 330, 390);

        BufferedImage newImage = FaceInvestigator.getFaceDrawing(bufferedImage, new FaceInvestigator.FaceDirections(rect, null));

        for (int i = 0; i < newImage.getWidth(); i++) {
            for (int j = 0; j < newImage.getHeight(); j++) {
                int rgb = newImage.getRGB(i, j);

                if(rgb != Color.BLACK.getRGB()){
                    bufferedImage.setRGB(i + rect.mRectX, j + rect.mRectY, rgb);
                }
            }
        }

        displayImage("Integrate", bufferedImage);
        displayImage("Face Map", newImage);
    }

    public static void displayImage(String Name, BufferedImage Image){
        JFrame frame = new JFrame(Name);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.getContentPane().setLayout(new FlowLayout());

        frame.getContentPane().add(new JLabel(new ImageIcon(Image)));

        frame.pack();
        frame.setVisible(true);
    }

    public static void displayColorMap(String Name, int[][] ColorMap){
        BufferedImage colorMapImage = new BufferedImage(
                ColorMap.length,
                ColorMap[0].length,
                BufferedImage.TYPE_INT_RGB
        );

        for (int i = 0; i < ColorMap.length; i++) {
            for (int j = 0; j < ColorMap[i].length; j++) {
                colorMapImage.setRGB(
                        i,
                        j,
                        ColorMap[i][j]
                );
            }
        }

        displayImage(Name, colorMapImage);
    }

}
