
import Lib.MathR.MathR;

import java.awt.*;
import java.awt.image.BufferedImage;

public class FaceInvestigator {

    private static final int mFaceConfinesGradient = 80;
    private static final int mFaceConfinesNumber = 250;

    public static BufferedImage getFaceDrawing(BufferedImage Image, FaceDirections FaceDirections){
        int[][] faceColorMap = cutFaceDrawing(Image, FaceDirections.mFaceRect);

        Main.displayColorMap("Face Cut", faceColorMap);

        int[][][] faceConfines = getFaceConfines(faceColorMap);

        BufferedImage faceShapeImage = new BufferedImage(
                faceColorMap.length,
                faceColorMap[0].length,
                BufferedImage.TYPE_INT_RGB
        );

        int redColor = Color.RED.getRGB();
        for (int i = 0; i < faceConfines.length; i++) {
            for (int j = 0; j < faceConfines[i].length; j++) {
                int x = faceConfines[i][j][0],
                    y = faceConfines[i][j][1];

                if(x == -1 || y == -1){
                    continue;
                }

                faceShapeImage.setRGB(
                        x,
                        y,
                        redColor
                );
            }
        }

        return faceShapeImage;
    }

    /**
     * Investigates the face shape
     * @param FaceColorsMap The color map of the face
     * @return Array of [number of check][first or second point][x or y]
     */
    private static int[][][] getFaceConfines(int[][] FaceColorsMap){
        int faceColorsMapHeight = FaceColorsMap[0].length,
            faceColorsMapWidth = FaceColorsMap.length,
            faceColorsMapHalfWidth = FaceColorsMap.length / 2,
            leap = MathR.squareRoot(FaceColorsMap.length);

        int[][][] faceConfinesList = new int[mFaceConfinesNumber][2][2];

        for (int colorMapIndex = 0, i = 0;
             colorMapIndex < faceColorsMapHeight && i < mFaceConfinesNumber;
             colorMapIndex += faceColorsMapHeight / mFaceConfinesNumber, i++) {

            //Get the x of point number 0, from 0 index to the half of the color map
            faceConfinesList[i][0][0] = getFaceConfinesIndex(
                    FaceColorsMap,
                    colorMapIndex,
                    leap,
                    0,
                    faceColorsMapHalfWidth
            );

            //Get the x of point number 1, from the half of the color map index to the end
            faceConfinesList[i][1][0] = getFaceConfinesIndex(
                    FaceColorsMap,
                    colorMapIndex,
                    leap,
                    faceColorsMapWidth,
                    faceColorsMapHalfWidth
            );

            //Insert the y value of the two confines points
            faceConfinesList[i][0][1] = faceConfinesList[i][1][1] = colorMapIndex;

        }

        return faceConfinesList;
    }

    /**
     * Search for the index of confines in face color map layer
     * @param FaceColorMap The face color map
     * @param Y The Layer Index
     * @param Leaps The jump between the points
     * @param StartIndex The index to start in
     * @param EndIndex The index to end in
     * @return The index of the point or -1 if not found
     */
    private static int getFaceConfinesIndex(int[][] FaceColorMap, int Y, int Leaps, int StartIndex, int EndIndex){
        for (int i = EndIndex - Leaps - 1; i > StartIndex; i -= Leaps) {
            if(getDistanceColors(FaceColorMap[i][Y], FaceColorMap[i + Leaps][Y]) >= mFaceConfinesGradient){
                return i + Leaps;
            }
        }

        return -1;
    }

    /**
     * Cut the face part from the image and return color map array
     * @param Image The big image
     * @param FaceRect The rect where the face is
     * @return Color map array
     */
    private static int[][] cutFaceDrawing(BufferedImage Image, Rect FaceRect){
        int[][] colorsMapArray = new int[FaceRect.mRectWidth][FaceRect.mRectHeight];

        BufferedImage cutImage = Image.getSubimage(
                FaceRect.mRectX,
                FaceRect.mRectY,
                FaceRect.mRectWidth,
                FaceRect.mRectHeight
        );

        for (int i = 0; i < FaceRect.mRectWidth; i++) {
            for (int j = 0; j < FaceRect.mRectHeight; j++) {
                colorsMapArray[i][j] = cutImage.getRGB(i, j);
            }
        }
        
        return colorsMapArray;
    }

    /**
     * Calculate the distance between two colors
     * @param ColorA The first color
     * @param ColorB The second color
     * @return The distance between the colors
     */
    private static int getDistanceColors(int ColorA, int ColorB){
        //According to d=sqrt((r2-r1)^2+(g2-g1)^2+(b2-b1)^2)

        return MathR.squareRoot(
           MathR.squarePower(getRed(ColorB)     - getRed(ColorA)) +
                 MathR.squarePower(getGreen(ColorB)   - getGreen(ColorA)) +
                 MathR.squarePower(getBlue(ColorB)    - getBlue(ColorA))
        );
    }

    /**
     * Get Red color number from RGB
     * @param RGB The color
     * @return Red value
     */
    private static int getRed(int RGB){
        return (RGB & 0xFF0000) >> 16;
    }

    /**
     * Get Green color number from RGB
     * @param RGB The color
     * @return Green value
     */
    private static int getGreen(int RGB){
        return (RGB & 0x00FF00) >> 8;
    }

    /**
     * Get Blue color number from RGB
     * @param RGB The color
     * @return Blue value
     */
    private static int getBlue(int RGB){
        return (RGB & 0x0000FF);
    }

    /**
     * Hold Face Directions Rect And Revolves Ellipse
     */
    public static class FaceDirections {
        private Rect mFaceRect;
        private Ellipse mFaceEllipse;

        public FaceDirections(Rect FaceRect, Ellipse FaceEllipse){
            this.mFaceRect = FaceRect;
            this.mFaceEllipse = FaceEllipse;

        }
    }

    /**
     * Hold Rect
     */
    public static class Rect {
        public int mRectX;
        public int mRectY;

        public int mRectWidth;
        public int mRectHeight;

        public Rect(int X, int Y, int Width, int Height){
            this.mRectX = X;
            this.mRectY = Y;
            this.mRectWidth = Width;
            this.mRectHeight = Height;

        }

    }

    /**
     * Hold Ellipse
     */
    public static class Ellipse {
        public int mCenterX;
        public int mCenterY;

        public int mA;
        public int mB;

        public Ellipse(int CenterX, int CenterY, int A, int B){
            this.mCenterX = CenterX;
            this.mCenterY = CenterY;
            this.mA = A;
            this.mB = B;

        }

    }

}
